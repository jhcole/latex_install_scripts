#!/bin/bash

if command -v potrace >/dev/null 2>&1; then
  echo "potrace already installed";
else
  # install potrace
  mkdir /tmp/potrace;

  cd /tmp/potrace && curl -sL https://bitbucket.org/jhcole/dpic/downloads/potrace-1.13.tar.gz | tar xz --strip-components=1 && ./configure --with-libpotrace && make && sudo make install && export LD_LIBRARY_PATH="/usr/local/lib";

  if [ ! -e "/etc/ld.so.conf.d/potraceLibs.conf" ]; then
    printf '"/usr/local/lib"' > /etc/ld.so.conf.d/potraceLibs.conf && ldconfig;
  fi

  rm -rf /tmp/potrace;
fi
exit 0;
