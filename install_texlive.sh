#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/tmp/texlive-deploy.log 2>&1

if command -v tlmgr >/dev/null 2>&1; then
  echo "texlive with tlmgr already installed";
else
  if command -v latex >/dev/null 2>&1; then
    # remove old repo version of texlive
    yum -y autoremove texlive-latex-bin texlive-texconfig-bin texlive-kpathsea-lib
  fi

  # minimally instll texlive from CTAN
  # uninstall with > tlmgr uninstall
  mkdir /tmp/texlive

  cd /tmp/texlive && curl -sL http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz | tar xz --strip-components=1 &&

  printf "%s\n" \
    "S" "d" "R" \
    "O" "D" "S" "L" "" "" "" "R" \
    "I" | perl install-tl

  # install extra packages I know we need
  /usr/local/bin/tlmgr install dvisvgm texliveonfly l3kernel l3packages xifthen ifmtarg jknapltx psfrag datetime etoolbox fmtcount subfigure rotating pgf ms xcolor placeins steinmetz pict2e tikz-qtree rsfs
  /usr/local/bin/tlmgr path add

  rm -rf /tmp/texlive
fi
