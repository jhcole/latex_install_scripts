.PS
cct_init

elen = 1
Origin: Here

source(up_ 1*elen with .start at Origin,I); llabel(,\mathbf{\tilde{I}_{s1}}=1\phase{60^\circ},) 
resistor(right_ 1*elen,E); llabel(,`3\angle{35^{\circ}} \, \Omega',)
{resistor(down_ elen,E); llabel(,`2\angle{45^{\circ}} \, \Omega',) rlabel(+,\mathbf{\tilde{V}_{out}},-) }
resistor(right_ 1*elen,E); llabel(,`4\angle{20^{\circ}} \, \Omega',)
source(down_ 1*elen ,I); llabel(,\mathbf{\tilde{I}_{s2}}=5\angle{30^\circ} ,) 
line to Origin
ground(at last line.c)

.PE
